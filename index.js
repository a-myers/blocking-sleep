module.exports = function(secs) {
    var waitTill = new Date(new Date().getTime() + secs * 1000);
    while(waitTill > new Date()){}
}