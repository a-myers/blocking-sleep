# blocking-sleep

NodeJS Library to provide simple sleep functionality. Designed to replicate Python's Time.sleep() function.

# Installation
```
npm install blocking-sleep
```

# Usage

First import the library...
```javascript
const sleep = require('blocking-sleep');
```

Then use the sleep(seconds) function anywhere in your code! sleep() is blocking, so node will not move on until the specified time has elapsed.

```javascript
let start = new Date();
sleep(5);
let end = new Date();

let diff = end - start //=> 5000 (milliseconds)
```

If you would like to sleep for a time shorter than a second, you can pass in floating point values to specify a sleeptime in milliseconds!
```javascript
let start = new Date();
sleep(.500);
let end = new Date();

let diff = end - start //=> 500 (milliseconds)
```

### That's it!

